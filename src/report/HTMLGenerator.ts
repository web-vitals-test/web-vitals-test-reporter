import * as fs from 'fs';
import {Scenario} from 'web-vitals-test/lib/Report/Scenario'
import { Aggregation, ReportLine, FID, CLS, LCP, Timings } from './../types';


export class HTMLGenerator {
  static fileName: string = 'HTMLReport/report.html';
  static body: string;

  static generateHTMLReport(
    reportLines: Scenario[],
    title: string,
    aggregatedData: { overall: Aggregation; CLS: Aggregation; LCP: Aggregation; FID: Aggregation },
  ) {
    HTMLGenerator.body = '<h1 style= "text-align: center;">Web Vitals Test Report : ' + title + ' </h1> \n';
    HTMLGenerator.body +=
      ' <div style="height: 5%; max-width: 10%; margin-left: 10%; "></div> \n \
                                <div style="height: 5%; max-width: 80%; " class="container"> \n \
                                    <div id="OverallChart" class="fixed"></div> \n \
                                    <div id="CLSChart" class="fixed-item tdCLS"></div> \n \
                                    <div id="LCPChart" class="fixed-item tdLCP"></div>  \n \
                                    <div id="FIDChart" class="fixed-item tdFID"></div>  \n \
                                </div>';
    HTMLGenerator.body += '<table id="Web_Vitals_Table"> \n';

    HTMLGenerator.body +=
      ' <tr> \n \
                                        <th class="tdScenario"> Scenario Name </th> \n \
                                        <th style="width: 21%;" colspan="3"> CLS </th> \n \
                                        <th style="width: 21%;" colspan="3"> LCP </th> \n \
                                        <th style="width: 21%;" colspan="3"> FID </th> \n \
                                </tr> \n';
    HTMLGenerator.body +=
      ' <tr> \n \
                                <th class="tdScenario"> </th> \n \
                                <th class="tdExperience"> Fast </th> \n \
                                <th class="tdExperience"> Average </th> \n \
                                <th class="tdExperience"> Poor </th> \n \
                                <th class="tdExperience"> Fast </th> \n \
                                <th class="tdExperience"> Average </th> \n \
                                <th class="tdExperience"> Poor </th> \n \
                                <th class="tdExperience"> Fast </th> \n \
                                <th class="tdExperience"> Average </th> \n \
                                <th class="tdExperience"> Poor </th> \n \
                        </tr> \n';
    for (let i = 0; i < reportLines.length; i++) {
      const scenario = reportLines[i];
      const scenarioLines = scenario.getEntryList();
      const scenarioData = scenario.getAggregatedData();
      HTMLGenerator.body +=
        ' <tr> \n \
                                        <td class="tdScenario"> <a id="Scenario_' +
        i +
        '" href="#" onclick="toggleScenario(' +
        i +
        ');">' +
        scenario.getName() +
        '</a></td> \n \
                                        ' +
        HTMLGenerator.getAggregationText(scenarioData.CLS.fast, 'fast') +
        ' \n \
                                        ' +
        HTMLGenerator.getAggregationText(scenarioData.CLS.average, 'average') +
        ' \n \
                                        ' +
        HTMLGenerator.getAggregationText(scenarioData.CLS.poor, 'poor') +
        ' \n \
                                        ' +
        HTMLGenerator.getAggregationText(scenarioData.LCP.fast, 'fast') +
        ' \n \
                                        ' +
        HTMLGenerator.getAggregationText(scenarioData.LCP.average, 'average') +
        ' \n \
                                        ' +
        HTMLGenerator.getAggregationText(scenarioData.LCP.poor, 'poor') +
        ' \n \
                                        ' +
        HTMLGenerator.getAggregationText(scenarioData.FID.fast, 'fast') +
        ' \n \
                                        ' +
        HTMLGenerator.getAggregationText(scenarioData.FID.average, 'average') +
        ' \n \
                                        ' +
        HTMLGenerator.getAggregationText(scenarioData.FID.poor, 'poor') +
        ' \n \
                                    </tr> \n';
      if (scenarioLines.length > 0) {
        HTMLGenerator.body += '<tr style="display:none;" id="ScenarioTable_' + i + '"> <td colspan="11">\n ';
        HTMLGenerator.body += '<table class="tableInner"> \n';
        HTMLGenerator.body +=
          '<tr> \n \
                                            <th class="tdAction thInner"> URL </th> \n \
                                            <th class="tdCLS thInner"> CLS </th> \n \
                                            <th class="tdLCP thInner"> LCP </th> \n \
                                            <th class="tdFID thInner"> FID </th> \n \
                                    </tr> \n';
        for (let j = 0; j < scenarioLines.length; j++) {
          HTMLGenerator.body +=
            '<tr> \n \
                                            <td class="tdAction"> <a href="#" id="show_' +
            i +
            '_' +
            j +
            '" onclick="toggle(' +
            i +
            ',' +
            j +
            ');"> ' +
            scenarioLines[j].URL +
            '    </a> <button onclick="document.getElementById(\'actions_' +
            i +
            '_' +
            j +
            '_action\').style.display=\'block\'" class="w3-button w3-black rightfloat">Timings</button> \n \
          <div id="actions_' +
            i +
            '_' +
            j +
            '_action" class="w3-modal"> \n \
            <div class="w3-modal-content"> \n \
              <div class="w3-container"> \n \
                <span onclick="document.getElementById(\'actions_' +
            i +
            '_' +
            j +
            '_action\').style.display=\'none\'" class="w3-button w3-display-topright">&times;</span> \n \
               ' +
            HTMLGenerator.getNavigationData(scenarioLines[j].navigationTimings) +
            ' \n \
              </div> \n \
            </div> \n   </td>' +
            HTMLGenerator.getValueText(scenarioLines[j].CLS, 'CLS', i, j) +
            ' \n \
                                            ' +
            HTMLGenerator.getValueText(scenarioLines[j].LCP, 'LCP', i, j) +
            ' \n \
                                            ' +
            HTMLGenerator.getValueText(scenarioLines[j].FID, 'FID', i, j) +
            ' \n \
                                        </tr> \n';
        }
        HTMLGenerator.body += '</table> \n';
        HTMLGenerator.body += '</td> </tr>\n ';
      }
    }
    HTMLGenerator.body += '</table> \n';

    const document = HTMLGenerator.buildHtml(HTMLGenerator.body, aggregatedData);

    if (!fs.existsSync('HTMLReport')) fs.mkdirSync('HTMLReport');
    fs.writeFileSync(this.fileName, document);
  }
  static getNavigationData(navigationTimings: Timings | undefined) {
    let navigationData = ' <p> Navigation Timings </p>';
    navigationData += '<p>---------------------------------------------------------------------</p>';

    if (navigationTimings) {
      Object.entries(navigationTimings).forEach(
        ([key, value]) => (navigationData += '<p>  ' + key + '    :    ' + value + '  </p>'),
      );
    }
    return navigationData;
  }

  static buildHtml(
    body: string,
    aggregatedData: { overall: Aggregation; CLS: Aggregation; LCP: Aggregation; FID: Aggregation },
  ) {
    const header =
      '\n    <meta name="viewport" content="width=device-width, initial-scale=1"> \n \
                            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> \n \
                            <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> \n \
                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> \n \
                            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> \n \
                            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> \n \
                            <script type="text/javascript"> google.charts.load("current", {"packages":["corechart"]}); \n \
                            google.charts.setOnLoadCallback(drawChart); \n \
                            function drawChart() { \n \
                            var overallData = google.visualization.arrayToDataTable([["Experience", "Distribution"],["Fast", ' +
      aggregatedData.overall.fast +
      '],["Average", ' +
      aggregatedData.overall.average +
      '],["Poor", ' +
      aggregatedData.overall.poor +
      ']]); \n\
                            var overallOptions = {"title":"OverAll",colors: ["#238823", "#FFBF00", "#D2222D"]}; \n \
                            var overallChart = new google.visualization.PieChart(document.getElementById("OverallChart")); \n \
                            overallChart.draw(overallData, overallOptions); \n \
                            var clsData = google.visualization.arrayToDataTable([["Experience", "Distribution"],["Fast", ' +
      aggregatedData.CLS.fast +
      '],["Average", ' +
      aggregatedData.CLS.average +
      '],["Poor", ' +
      aggregatedData.CLS.poor +
      ']]); \n\
                            var clsOptions = {"title":"CLS",legend: "none", pieSliceText: "label",colors: ["#238823", "#FFBF00", "#D2222D"]}; \n \
                            var clsChart = new google.visualization.PieChart(document.getElementById("CLSChart")); \n \
                            clsChart.draw(clsData, clsOptions); \n \
                            var lcpData = google.visualization.arrayToDataTable([["Experience", "Distribution"],["Fast", ' +
      aggregatedData.LCP.fast +
      '],["Average", ' +
      aggregatedData.LCP.average +
      '],["Poor", ' +
      aggregatedData.LCP.poor +
      ']]); \n\
                            var lcpOptions = {"title":"LCP",legend: "none", pieSliceText: "label",colors: ["#238823", "#FFBF00", "#D2222D"]}; \n \
                            var lcpChart = new google.visualization.PieChart(document.getElementById("LCPChart")); \n \
                            lcpChart.draw(lcpData, lcpOptions); \n \
                            var fidData = google.visualization.arrayToDataTable([["Experience", "Distribution"],["Fast", ' +
      aggregatedData.FID.fast +
      '],["Average", ' +
      aggregatedData.FID.average +
      '],["Poor", ' +
      aggregatedData.FID.poor +
      ']]); \n\
                            var fidOptions = {"title":"FID",legend: "none", pieSliceText: "label",colors: ["#238823", "#FFBF00", "#D2222D"]}; \n \
                            var fidChart = new google.visualization.PieChart(document.getElementById("FIDChart")); \n \
                            fidChart.draw(fidData, fidOptions);} \n \
                            function toggle(i,j){ \n \
                                console.log("index is " + i) \n \
                                let actionElement =document.querySelector("#show_"+i+"_"+j) \n \
                                console.log(actionElement) \n \
                                actionElement.scrollIntoView() \n \
                                let clsImageElement = document.querySelector("#Extra_"+i+"_"+j+"_CLS"); \n \
                                console.log(clsImageElement) \n \
                                if(clsImageElement) { clsImageElement.style.display = clsImageElement.style.display == "none" ? "inherit" : "none"} \n \
                                let lcpImageElement = document.querySelector("#Extra_"+i+"_"+j+"_LCP"); \n \
                                console.log(lcpImageElement) \n \
                                if(lcpImageElement) { lcpImageElement.style.display = lcpImageElement.style.display == "none" ? "inherit" : "none"} \n \
                            } \n \
                            function toggleScenario(index){ \n \
                                console.log("index is " + index) \n \
                                let actionElement =document.querySelector("#Scenario_"+index) \n \
                                actionElement.scrollIntoView() \n \
                                let scenarioTable = document.querySelector("#ScenarioTable_"+index); \n \
                                if(scenarioTable) { scenarioTable.style.display = scenarioTable.style.display == "none" ? "" : "none" } \n \
                            } \n \
                            </script> \n \
                            <style> \n \
                            table, th, td { border: 1px solid black; text-align: center; } \n \
                             table { border-spacing: 5px; margin-left: 10% ; width: 80%; } \n \
                            .tableInner {width: 90%; margin-left: 10%; font-size: 12px;} \n \
                             th{font-size: 18px;} \n \
                             .thInner{font-size: 12px;} \n \
                             .tdAction {width: 25%; padding-left: 10px; text-align: left;  } \n \
                             .tdScenario {width:34%; padding-left: 10px; text-align: left;  font-size: 16px;} \n \
                             .tdCLS {width: 21%; } \n \
                             .tdExperience {width: 7%; font-size: 14px;} \n \
                             .tdFID {width: 21%; } \n \
                             .tdLCP {width: 21%; } \n \
                             .tdURL {width: 19%; } \n \
                             .tdShow {width:4%; font-size: 12px;} \n \
                             .fast{background: #238823; color:white} \n \
                             .average{background: #FFBF00; color:white} \n \
                             .poor{background: #D2222D; color:white} \n \
                             .container{display: flex;} \n \
                             .fixed{width: 34%;} \n \
                             .fixed-item{flex-grow: 1;} \n \
                             .hidden {display: none} \n \
                             .rightfloat {float : right; padding: unset;} \n \
                            </style>';

    return '<!DOCTYPE html>' + '<html><head>' + header + '</head><body>' + body + '</body></html>';
  }

  static getValueText(
    metric: ReportLine['CLS'] | ReportLine['LCP'] | ReportLine['FID'],
    type: string,
    i: number,
    j: number,
  ) {
    let AdditionalData = '';
    if (metric) {
      if (metric.screenshot) {
        AdditionalData =
          '<div style="display: none;" id="Extra_' +
          i +
          '_' +
          j +
          '_' +
          type +
          '"><a href="../' +
          metric.screenshot +
          '"><img src="../' +
          metric.screenshot +
          '">  </a> ' +
          '<button onclick="document.getElementById(\'elements_' +
          i +
          '_' +
          j +
          '_' +
          type +
          '\').style.display=\'block\'" class="w3-button w3-black">Show Elements</button> \n \
        <div id="elements_' +
          i +
          '_' +
          j +
          '_' +
          type +
          '" class="w3-modal"> \n \
          <div class="w3-modal-content"> \n \
            <div class="w3-container"> \n \
              <span onclick="document.getElementById(\'elements_' +
          i +
          '_' +
          j +
          '_' +
          type +
          '\').style.display=\'none\'" class="w3-button w3-display-topright">&times;</span> \n \
             ' +
          HTMLGenerator.getModalData(metric, type) +
          ' \n \
            </div> \n \
          </div> \n \
        </div> </div>';
      }
      if ((metric as FID).startTime) {
        AdditionalData = ' ( event time : ' + (metric as FID).startTime + ' )';
      }
    }
    if (metric && metric.hasOwnProperty('value'))
      if (type === 'FID')
        return (
          '<td > <div class="' + metric.experience + '">' + metric.value + ' ms ' + AdditionalData + '  </div>  </td>'
        );
      else
        return '<td > <div class="' + metric.experience + '">' + metric.value + '</div> ' + AdditionalData + ' </td>';
    else return '<td></td>';
  }

  static getAggregationText(value: number, experience: string) {
    let background = 'fast';
    if (experience === 'average') {
      if (value === 0) background = 'fast';
      else background = 'average';
    }

    if (experience === 'poor') {
      if (value === 0) background = 'fast';
      else background = 'poor';
    }

    if (value === 0) background = '';
    return '<td class="tdExperience ' + background + '"> ' + value + '</td>';
  }

  static getModalData(data: CLS | LCP | FID, type: string) {
    let elementHTML;
    if (type === 'CLS') {
      elementHTML = '<p> CLS Elements </p> \n ';
      elementHTML +=
        '<p>-----------------------------------------------------------------------------------------------------------</p> \n';
      const clsMetric: CLS = data as CLS;
      if (clsMetric.elements) {
        for (const clsElement of clsMetric.elements) {
          elementHTML += '<p>' + clsElement.cssSelector + '</p> \n';
        }
      }
    } else if (type === 'LCP') {
      elementHTML = '<p> LCP Element </p> \n ';
      elementHTML +=
        '<p>-----------------------------------------------------------------------------------------------------------</p> \n';
      const lcpMetric: LCP = data as LCP;
      if (lcpMetric.element) {
        elementHTML += '<p>' + lcpMetric.element.cssSelector + '</p> \n';
      }
    } else if (type === 'FID') {
      elementHTML = '<p> FID data </p> \n ';
      elementHTML +=
        '<p>-----------------------------------------------------------------------------------------------------------</p> \n';
      const fidMetric: FID = data as FID;
      if (fidMetric.startTime) {
        elementHTML += '<p> FID start Time : ' + fidMetric.startTime + '</p> \n';
      }
    }
    return elementHTML;
  }
}

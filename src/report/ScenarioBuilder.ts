import { PathLike } from "fs";

import * as fs from 'fs'
import {Scenario} from 'web-vitals-test/lib/Report/Scenario'
import { URLReport } from "web-vitals-test/lib/Report/URLReport";

export class ScenarioBuilder{
    static scenarioList: Scenario[] = []
    static aggregatedData:any
    static buildFromFile(path: PathLike){
        const inputText = fs.readFileSync(path+'/webvitals.json').toString();
        const inputJSON = JSON.parse(inputText)

       for(const result of inputJSON.results){
           const scenario = new Scenario(result.name,0);
           const entries : URLReport[] = result.URLList
           const urlReportList: URLReport[] = []
           for(const entry of entries){
                const urlReport = new URLReport(entry.URL)
                urlReport.actionList = entry.actionList
                urlReport.CLS = entry.CLS
                urlReport.FID = entry.FID
                urlReport.LSN = entry.LSN
                urlReport.LCP = entry.LCP
                urlReport.navigationTimings = entry.navigationTimings
                urlReportList.push(urlReport)
           }
           scenario.setURLList(urlReportList)
           ScenarioBuilder.scenarioList.push(scenario);
       }

       ScenarioBuilder.aggregatedData= inputJSON.aggregation
    }
}
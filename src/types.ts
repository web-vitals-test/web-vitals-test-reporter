import { PathLike } from "fs";

export interface CLSData {
  value: number;
  entries?: [{ sources: TestElement[] }];
  action?: string;
}

export interface TestElement {
  cssSelector?: string;
  location?: Location;
  newLocation?: Location;
  previousRect?: Location;
}

export interface FIDData {
  value: number;
  entries?: [{ startTime?: number }];
  action?: string;
}

export interface LCPData {
  value: number;
  entries?: [];
  action?: string;
}

export interface WebVitalsTestResult {
  action: string;
  CLS: CLSData;
  FID: FIDData;
  LCP: LCPData;
}

export interface LcpAPIEntry {
  loadTime: number;
  renderTime: number;
  url: string;
  id: string;
  cssSelector: string;
  location: Location;
}

export interface LcpAPI {
  name: string;
  value: number;
  delta?: number;
  entries?: LcpAPIEntry[];
  id?: string;
  action?: string;
}

export interface Location {
  x: number;
  y: number;
  width: number;
  height: number;
  top?: number;
  right?: number;
  bottom?: number;
  left?: number;
}

export interface CLS {
  value: number;
  elements?: TestElement[];
  experience?: Experience;
  screenshot?: string;
}

export interface LCP {
  value: number;
  element?: TestElement;
  experience?: Experience;
  screenshot?: string;
}

export interface FID {
  value: number;
  startTime?: number;
  experience?: Experience;
  screenshot?: { actual: string; reduced: string };
}

export interface ReportEntry {
  actionName: string;
  URL: string;
  CLS?: CLS;
  FID?: FID;
  LCP?: LCP;
  navigationTimings?: Timings;
}

export interface ReportLine {
  actionName: string;
  URL: string;
  CLS?: CLS;
  LCP?: LCP;
  FID?: FID;
  navigationTimings?: Timings;
  screenshot?: string;
}

export interface Aggregation {
  [Experience.fast]: number;
  [Experience.average]: number;
  [Experience.poor]: number;
}

export enum Experience {
  fast = 'fast',
  average = 'average',
  poor = 'poor',
}

export interface WaitOptions {
  waitFor: number;
}

export interface Timings {
  requestStart: number;
  responseStart: number;
  responseEnd: number;
  domInteractive: number;
  domContentLoadedEventStart: number;
  domContentLoadedEventEnd: number;
  domComplete: number;
  loadEventStart: number;
  loadEventEnd: number;
}

interface Arguments {
  path : PathLike,
}
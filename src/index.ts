#!/usr/bin/env node

import yargs = require('yargs/yargs');
import * as fs from 'fs'
import {ScenarioBuilder} from './report/ScenarioBuilder'
import {HTMLGenerator} from './report/HTMLGenerator'

const argv = yargs(process.argv.slice(2)).options({
    Path : { type : 'string' },
    ReportName : {type : 'string' , default: ' Test'}
}).argv
if(!argv.Path){
    console.error('Path is required')
    process.exit(1);
}

ScenarioBuilder.buildFromFile(argv.Path);
HTMLGenerator.generateHTMLReport(ScenarioBuilder.scenarioList,argv.ReportName,ScenarioBuilder.aggregatedData)
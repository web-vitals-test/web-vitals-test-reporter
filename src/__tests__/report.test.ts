
import {ScenarioBuilder} from '../report/ScenarioBuilder'
import {HTMLGenerator} from '../report/HTMLGenerator'
import {expect} from 'chai'

describe('Report', function () {
  it('Build scenario List', () => {
    ScenarioBuilder.buildFromFile('.');
    expect(ScenarioBuilder.scenarioList).length.greaterThan(0)
  });

  it.only('Create Report', () => {
    ScenarioBuilder.buildFromFile('.');
    HTMLGenerator.generateHTMLReport(ScenarioBuilder.scenarioList,"My Report",ScenarioBuilder.aggregatedData)
  });
});

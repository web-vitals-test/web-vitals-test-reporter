# Web Vitals Test Reporter

Reporter for web-vitals-test

## How to use

Run test using web-vitals-test

Install web-vitals-test-reporter

Run web-vitals-test-reporter --path ./report

report folder must be the output from web-vitals-test.

HTML report is created under HTMLReport folder
